package edu.nyu.cs.cs2580;

import java.io.IOException;

public class Testing {
	
	public static void main(String[] args) throws IOException {
		//testInvertedIndexDocOnly();
		//testQueryPhrase();
		//testInvertedIndexOccurence();
	}
	
	public static void testInvertedIndexDocOnly() throws IOException {
		IndexerInvertedDoconly test = new IndexerInvertedDoconly();
		long startTime = System.currentTimeMillis();
		test.constructIndex();
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
    System.out.println(elapsedTime / 1000);
				
		test.dumpInvertedIndex();
		
		test.dumpDictionary();
		
		test.dumpTermCorpusFrequency();
		
		test.dumpTermDocFrequency();
		
		Query query = new Query("anuj");
		query.processQuery();
		
		Document d = test.nextDoc(query, -1);
		
		if (d == null) {
			System.out.println("No document found");
		} else {
			printDocument(d);
		}
		System.out.println("Terms 'bora' in docid 1 = " + test.documentTermFrequency("sdsfs", 1));
	}
	
	private static void printDocument(Document doc) {
		System.out.println("Document Title : " + doc.getTitle());
		System.out.println("Document URL : " + doc.getUrl());
		System.out.println("Document Views : " + doc.getNumViews());
	}
	
	public static void testQueryPhrase() throws IOException {
		QueryPhrase qp = new QueryPhrase("\"New York University\" is located in \"New York City\"");
		qp.processQuery();
		
		QueryPhrase qp2 = new QueryPhrase("This is a \"random query\" and only thing \"random in this\" query is the repeated and random use \"of the word\" random");
		qp2.processQuery();
		
		//System.out.println(qp.getPhraseQueries());
		System.out.println(qp2.getPhraseQueries());
		System.out.println(qp2._tokens);
	}
	
	public static void testInvertedIndexOccurence() throws IOException {
		IndexerInvertedOccurrence test = new IndexerInvertedOccurrence();
		long startTime = System.currentTimeMillis();
		test.constructIndex();
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
    System.out.println(elapsedTime / 1000);
	}
	
}
