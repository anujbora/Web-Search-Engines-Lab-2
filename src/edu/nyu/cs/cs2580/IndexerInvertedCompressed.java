package edu.nyu.cs.cs2580;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Implement this class for HW2.
 */
public class IndexerInvertedCompressed extends Indexer implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 8431981064333731666L;
  //Maps each term to their integer representation
  private Map<String, Integer> _dictionary = new HashMap<String, Integer>();
  // All unique terms appeared in corpus. Offsets are integer representations.
  private Vector<String> _terms = new Vector<String>();

  private Map<Integer, IndexerInvertedCompressedPosting> _termOccurences = 
      new HashMap<Integer, IndexerInvertedCompressedPosting>();
  private Map<Character, Map<Integer, IndexerInvertedCompressedPosting>> _termOccurencesSplit = 
      new HashMap<Character, Map<Integer, IndexerInvertedCompressedPosting>>();
  private Map<Character, Map<Integer, IndexerInvertedCompressedPosting>> _termOccurencesSplitChars = 
      new HashMap<Character, Map<Integer, IndexerInvertedCompressedPosting>>();
  
  // Store all documents in memory.
  private Vector<Document> _documents = new Vector<Document>();
  
  //Term document frequency, key is the integer representation of the term and
  // value is the number of documents the term appears in.
  private Map<Integer, Integer> _termDocFrequency =
     new HashMap<Integer, Integer>();
  // Term frequency, key is the integer representation of the term and value is
  // the number of times the term appears in the corpus.
  private Map<Integer, Integer> _termCorpusFrequency =
     new HashMap<Integer, Integer>();

  // Provided for serialization.
  public IndexerInvertedCompressed() { }
  
  public IndexerInvertedCompressed(Options options) {
    super(options);
    System.out.println("Using Indexer: " + this.getClass().getSimpleName());
  }
  
  @Override
  public void constructIndex() throws IOException {
    RawDocumentParser parser = new RawDocumentParser();
    String corpusFile = _options._corpusPrefix;
    System.out.println("Constructing index documents in: " + corpusFile);
    
    File folder = new File(corpusFile);
    for (File file : folder.listFiles()) {
       // dont read hidden files / directories
      if (file.isHidden() || file.isDirectory() || !file.isFile()) {
        continue; 
      }
      
      // Parse Sample file
      if (file.getName().endsWith("corpus.tsv") ) {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        while ((line = reader.readLine()) != null) {
          line = parser.getConvertedDocument(line);
          processDocument(line);      
        }
      
      reader.close();
      } else { 
        String doc = parser.getConvertedDocument(file);
        processDocument(doc);
        
        if(_documents.size() % 1000 == 0){
          System.out.println("Processed " + _documents.size() + " Documents.");
        }
      }
    }
    
    for(String word : _dictionary.keySet()){
      Integer wordNum = _dictionary.get(word);
      IndexerInvertedCompressedPosting posting = _termOccurences.get(wordNum);
      Character firstChar = word.charAt(0);
      if(_termOccurencesSplit.containsKey(firstChar)) {
        Map<Integer, IndexerInvertedCompressedPosting> _termOccurencesChar = 
            _termOccurencesSplit.get(firstChar);
        _termOccurencesChar.put(wordNum, posting);
      }
      else{
        Map<Integer, IndexerInvertedCompressedPosting> _termOccurencesChar = 
            new HashMap<Integer, IndexerInvertedCompressedPosting>();
        _termOccurencesChar.put(wordNum, posting);
        _termOccurencesSplit.put(firstChar, _termOccurencesChar);
      }
    }
    
    System.out.println(
        "Indexed " + Integer.toString(_numDocs) + " docs with " +
        Long.toString(_totalTermFrequency) + " terms.");

    System.out.println("Store index to: " + _options._indexPrefix);  
    //int i = 0;
    //int allsize = _termOccurencesSplit.size();
    for(char alphabet: _termOccurencesSplit.keySet()) {
      String indexPostingFile = _options._indexPrefix + "/corpus_" + alphabet +".idx";
      //System.out.println("Store index to: " + indexPostingFile);
      ObjectOutputStream writer =
          new ObjectOutputStream(new FileOutputStream(indexPostingFile));
      Map<Integer, IndexerInvertedCompressedPosting> termOccurencesForChar = _termOccurencesSplit.get(alphabet);
      for (Integer key: termOccurencesForChar.keySet()) {
        termOccurencesForChar.get(key).encode();
      }
      writer.writeObject(termOccurencesForChar);
      writer.close();
      
      //i++;
      //int value = (int)((float)i*100.0/allsize*100);
      //float va = (float) ((float)value/100.0);
      //String output = "Storing index: " + va + "%";
      //System.out.println(output);
    }
    
    String indexPostingFile = _options._indexPrefix + "/corpus.idx";
    System.out.println("Stored index at: " + _options._indexPrefix);
    this._termOccurencesSplit.clear();;
    this._termOccurences.clear();
    ObjectOutputStream writer =
        new ObjectOutputStream(new FileOutputStream(indexPostingFile));
      writer.writeObject(this);
      writer.close();
  }
  
  public void processDocument(String content) {
    
    Scanner s = new Scanner(content);
    s.useDelimiter("\t");
    String title = s.next();
    String stemmedTitle = s.next();
    Map<Integer, Vector<Integer>> occurencesForDoc = 
        new HashMap<Integer, Vector<Integer>>();
    String body = s.next();
    String titleAndBody = stemmedTitle + " " + body;
    
    DocumentIndexed doc = new DocumentIndexed(_documents.size());
    
    doc.size = readTermVector(titleAndBody, occurencesForDoc);
    
    
    
    for(Integer key: occurencesForDoc.keySet()) {
      Vector<Integer> postingForDoc = occurencesForDoc.get(key); 
      if(_termOccurences.containsKey(key)) {
         IndexerInvertedCompressedPosting posting = _termOccurences.get(key); 
         posting.add(_documents.size(), postingForDoc);
         //System.out.println(key);
         //System.out.println(posting._posting);
         //break;
      }
      else {
        IndexerInvertedCompressedPosting posting = new IndexerInvertedCompressedPosting();
        posting.add(_documents.size(), postingForDoc);
        _termOccurences.put(key, posting);
      }
    }
    
    int numViews = Integer.parseInt(s.next());
    String url = s.next();
    s.close();

    doc.setTitle(title);
    doc.setNumViews(numViews);
    doc.setUrl(url);
    //doc.setTitleTokens(titleTokens);
    //doc.setBodyTokens(bodyTokens);
    _documents.add(doc);
    ++_numDocs;
    
    Set<Integer> uniqueTerms = new HashSet<Integer>();
    for (Integer idx : uniqueTerms) {
      _termDocFrequency.put(idx, _termDocFrequency.get(idx) + 1);
    }
  }
  
  /**
   * Tokenize {@code content} into terms, translate terms into their integer
   * representation, store the integers in {@code tokens}.
   * @param content
   * @param tokens
   */
  private int readTermVector(String content, Map<Integer, Vector<Integer>> occurrences) {
    Set<Integer> uniqueTerms = new HashSet<Integer>();
    
    Scanner s = new Scanner(content);  // Uses white space by default.
    int pos = 0;
    while (s.hasNext()) {
      String token = s.next();
      int idx = -1;
      if (_dictionary.containsKey(token)) {
        idx = _dictionary.get(token);
      } else {
        idx = _terms.size();
        _terms.add(token);
        _dictionary.put(token, idx);
        _termCorpusFrequency.put(idx, 0);
        _termDocFrequency.put(idx, 0);
      }
      if(occurrences.containsKey(idx)){
        Vector<Integer> occurrencesForAWord = occurrences.get(idx);
        occurrencesForAWord.add(pos);
      } else {
        Vector<Integer> occurrencesForAWord = new Vector<Integer>();
        occurrencesForAWord.add(pos);
        occurrences.put(idx, occurrencesForAWord);
      }
      pos++;
      _termCorpusFrequency.put(idx, _termCorpusFrequency.get(idx) + 1);
      ++_totalTermFrequency;
      
      uniqueTerms.add(idx);
    }
    for (Integer idx2 : uniqueTerms) {
      _termDocFrequency.put(idx2, _termDocFrequency.get(idx2) + 1);
    }
    s.close();
    return pos;
  }

  @SuppressWarnings("unchecked")
  @Override
  public void loadIndex() throws IOException, ClassNotFoundException {
    String indexFile = _options._indexPrefix + "/corpus.idx";
    System.out.println("Load index from: " + indexFile);

    ObjectInputStream reader =
        new ObjectInputStream(new FileInputStream(indexFile));
    IndexerInvertedCompressed loaded = (IndexerInvertedCompressed) reader.readObject();

    this._documents = loaded._documents;
    // Compute numDocs and totalTermFrequency b/c Indexer is not serializable.
    this._numDocs = _documents.size();
    for (Integer freq : loaded._termCorpusFrequency.values()) {
      this._totalTermFrequency += freq;
    }
    this._dictionary = loaded._dictionary;
    this._terms = loaded._terms;
    this._termCorpusFrequency = loaded._termCorpusFrequency;
    this._termDocFrequency = loaded._termDocFrequency;
    this._termOccurencesSplit = loaded._termOccurencesSplit;
    this._termOccurences = loaded._termOccurences;
    
    Set<Character> set = new HashSet<> ();
    set.add('a'); set.add('t'); set.add('s');
    set.add('c'); set.add('i'); set.add('p');
    set.add('b'); set.add('o'); set.add('m');
    set.add('w');
    for(Character ch : set){
      String idxFile = _options._indexPrefix + "/corpus_" + ch + ".idx";
      ObjectInputStream rdr;
      rdr = new ObjectInputStream(new FileInputStream(idxFile));
      Map<Integer, IndexerInvertedCompressedPosting> _termOccurencesChar = 
          (Map<Integer, IndexerInvertedCompressedPosting>)rdr.readObject();
      this._termOccurencesSplitChars.put(ch, _termOccurencesChar);
      rdr.close();
    }
    reader.close();
    
    
    System.out.println(Integer.toString(_numDocs) + " documents loaded " +
        "with " + Long.toString(_totalTermFrequency) + " terms!");
  }

  @Override
  public Document getDoc(int did) {
    return (did >= _documents.size() || did < 0) ? null : _documents.get(did);
  }
  
  public void unLoadUnnecessaryIndexer() {
    _termOccurencesSplit = null;
    _termOccurencesSplit = new HashMap<Character, Map<Integer, IndexerInvertedCompressedPosting>>
                            (_termOccurencesSplitChars);
  }
  
  @SuppressWarnings("unchecked")
  private void loadNecessaryIndexer(Vector<String> queryTokens) throws FileNotFoundException, IOException, ClassNotFoundException {
    for(String word: queryTokens) {
      if(!_termOccurencesSplit.containsKey(word.charAt(0))){
        unLoadUnnecessaryIndexer();
        break;
      }
    }
    
    for(String word : queryTokens) {
      if(!_termOccurencesSplit.containsKey(word.charAt(0))) {
        String indexFile = _options._indexPrefix + "/corpus_" +word.charAt(0)+ ".idx";
        ObjectInputStream reader;
        reader = new ObjectInputStream(new FileInputStream(indexFile));
        Map<Integer, IndexerInvertedCompressedPosting> _termOccurencesChar = 
            (Map<Integer, IndexerInvertedCompressedPosting>)reader.readObject();
        this._termOccurencesSplit.put(word.charAt(0), _termOccurencesChar);
        reader.close();
      }
    }
  }

  @Override
  public Document nextDoc(Query query, int docid) {
    QueryPhrase qp = ((QueryPhrase)query);
    Vector<String> queryTokens = qp._tokens;
    for(String token: queryTokens) {
      if(!_dictionary.containsKey(token)) {
        return null;
      }
    }
    try {
      loadNecessaryIndexer(queryTokens);
    } catch (ClassNotFoundException | IOException e) {
      e.printStackTrace();
    }
    boolean flag = true;
    int i = 0;
    int prevDocId = -1;
    int maxDocId = -1;
    // System.out.print("1");
    for(String token: queryTokens) {
      int idx = _dictionary.get(token);
      IndexerInvertedCompressedPosting posting = this._termOccurencesSplit.get(token.charAt(0)).get(idx);
      // System.out.print("1.1");
      int nextDocId = posting.next(docid);
      // System.out.print("1.2");
      //System.out.print(nextDocId);
      if(nextDocId == -1) {
        return null;
      }
      // System.out.print("Boom!");
      //System.out.print(nextDocId);
      if(i == 0) {
        prevDocId = nextDocId;
      } else if (prevDocId != nextDocId) {
        flag = false;
      } 
      // System.out.print("1.3");
      maxDocId = Math.max(maxDocId, nextDocId);
      prevDocId = nextDocId;
      i++;
    } 
    // System.out.print("2");
    if(flag == true) {
      // System.out.print("2.1");
      if(qp.phraseQueries != null && !qp.phraseQueries.isEmpty()) {
        // System.out.print("2.2");
        if(checkPhrases(qp.phraseQueries,maxDocId)) {
          // System.out.print("2.3");
          // System.out.print(maxDocId);
          
          return _documents.get(maxDocId);
        } else {
          // System.out.print("2.4");
          return nextDoc(query, maxDocId);
        }
        
      } else{
        return _documents.get(maxDocId);
      }
    }
    // System.out.print("3");
    return nextDoc(query, maxDocId-1);
  }
  

  public Boolean checkPhrases(Vector<Vector<String>> phrases, int docId){
    for(Vector<String> phrase : phrases) {
      if(!checkPhrase(phrase,docId)){
        return false;
      }
    }
    return true;
  }
  
  public Boolean checkPhrase(Vector<String> phrase, int docId) {
    //HashMap<Integer, Integer> cache = new HashMap<Integer, Integer>();
    Vector<Integer> checkPositions= new Vector<Integer>();
    boolean flag = true;
    int prevPos = -1;
    checkPositions.add(-1);
    while(true) {
      flag = true;
      int i = 0;
      prevPos = checkPositions.get(0);
      for(String str: phrase){
        int idx = _dictionary.get(str);
        if(prevPos == -1 || checkPositions.get(i) <= prevPos) {
          IndexerInvertedCompressedPosting posting = _termOccurencesSplit.get(str.charAt(0)).get(idx);
          int newPos = posting.next_pos(docId, prevPos);
          if(newPos == -1) {
            return false;
          }
          checkPositions.add(i,newPos);
        }
        if(i!=0) {
          if(checkPositions.get(i-1) == checkPositions.get(i)) {
            IndexerInvertedCompressedPosting posting = _termOccurencesSplit.get(str.charAt(0)).get(idx);
            checkPositions.add(i,posting.next_pos(docId, checkPositions.get(i)));
          }
          if(checkPositions.get(i-1) + 1!= checkPositions.get(i)) {
            flag = false;
          }
        } else {
          prevPos = checkPositions.get(i);
        }
        i++;
      } 
      if(flag == true) {
        return true;
      }
    }
  }

  @Override
  public int corpusDocFrequencyByTerm (String term) {
    return _dictionary.containsKey(term) ?
        _termDocFrequency.get(_dictionary.get(term)) : 0;
  }

  @Override
  public int corpusTermFrequency(String term) {
    return _dictionary.containsKey(term) ?
       _termCorpusFrequency.get(_dictionary.get(term)) : 0;
  }

  @Override
  public int documentTermFrequency(String term, int docid) {
   if (!_dictionary.containsKey(term)) {
     return 0;
   }
   int idx = _dictionary.get(term);
    IndexerInvertedCompressedPosting posting = _termOccurencesSplit.get(term.charAt(0)).get(idx);
    //System.out.println(posting.totalTermsInDocument(docid));
    return posting.totalTermsInDocument(docid);
  }
  
  public Vector<String> getTermVector(Vector<Integer> tokens) {
    Vector<String> retval = new Vector<String>();
    for (int idx : tokens) {
      retval.add(_terms.get(idx));
    }
    return retval;
  }

}
