package edu.nyu.cs.cs2580;

import java.io.IOException;
import java.util.Arrays;
import java.util.Vector;

/**
 * @CS2580: implement this class for HW2 to handle phrase. If the raw query is
 * ["new york city"], the presence of the phrase "new york city" must be
 * recorded here and be used in indexing and ranking.
 */
public class QueryPhrase extends Query {

	public Vector<Vector<String>> phraseQueries = new Vector<Vector<String>>();

	public QueryPhrase(String query) {
    super(query);
  }

  @Override
  public void processQuery() {
  	String q = _query;
    q = q.replace('+', ' ');

    String[] phrases = q.split("\"");
    
    Vector<String> phrasesAll = new Vector<String>(Arrays.asList(phrases));
    RawDocumentParser stemmer = new RawDocumentParser();
    for (int i = 0; i < phrasesAll.size(); i++) {
    	String stemmedSubQuery = null;
    	try {
    		stemmedSubQuery = stemmer.cleanAndStem(phrasesAll.get(i));
			} catch (IOException ignore) {
				// ignore
			}
    	Vector<String> allTokens = new Vector<String>(Arrays.asList(stemmedSubQuery.split(" ")));
    	if (i % 2 == 1) {
    		phraseQueries.add(allTokens);
    	} 
    	for (String t : allTokens) {
    		if (!t.isEmpty()) {
    			_tokens.add(t);
    		}
    	}
    }
  }
  
  public Vector<Vector<String>> getPhraseQueries() {
		return phraseQueries;
	}
}
