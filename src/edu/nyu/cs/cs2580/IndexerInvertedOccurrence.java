package edu.nyu.cs.cs2580;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Vector;

import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Implement this class for HW2.
 */
public class IndexerInvertedOccurrence extends Indexer implements Serializable {

	private static final long serialVersionUID = 3488594845126558281L;

	// Maps each term to their integer representation
	public Map<String, Integer> _dictionary = new HashMap<String, Integer>();
	  
	// All unique terms appeared in corpus. Offsets are integer representations.
	//private Vector<String> _terms = new Vector<String>();

	// Term document frequency, key is the integer representation of the term and
	// value is the number of documents the term appears in.
	public Map<Integer, Integer> _termDocFrequency = new HashMap<Integer, Integer>();
	  
	// Term frequency, key is the integer representation of the term and value is
	// the number of times the term appears in the corpus.
	public Map<Integer, Integer> _termCorpusFrequency = new HashMap<Integer, Integer>();

	// Stores all Document in memory.
	public Vector<Document> _documents = new Vector<Document>();
	  
	// Posting lists of each term
	public TreeMap<Integer, Vector<Integer>> _postings=new TreeMap<Integer,Vector<Integer>>();
	  
	// Skip Pointers for each term
	public HashMap<Integer, Vector<Integer>> _skipPointers = new HashMap<Integer, Vector<Integer>>();
	
	public TreeMap<Integer, Vector<Integer>> _FixedPostings=new TreeMap<Integer,Vector<Integer>>();

	int tokenPosition = 0;
	
	int FIXED_SIZE = 7;
	
	int BUF_SIZE = 50;
	  
	// Provided for serialization
	public IndexerInvertedOccurrence() { }
	
	
	public IndexerInvertedOccurrence(Options options) {
		super(options);
		System.out.println("Using Indexer: " + this.getClass().getSimpleName());
	}
  
	@Override
	public void constructIndex() throws IOException {
	  
		RawDocumentParser parser = new RawDocumentParser();
	  	String corpusFile = _options._corpusPrefix;
	    System.out.println("Constructing index documents in: " + corpusFile);
	    
	    File folder = new File(corpusFile);
	    for (File file : folder.listFiles()) {
	    	if (file.isHidden() || file.isDirectory() || !file.isFile()) {
	    		continue;	
	    	}
	      
	    	// Parse Sample file
	    	if (file.getName().endsWith("corpus.tsv") ) {
	    		BufferedReader reader = new BufferedReader(new FileReader(file));
	    		String line = null;
	    		while ((line = reader.readLine()) != null) {
	    			line = parser.getConvertedDocument(line);
	    			processDocument(line); 			
	    		}
	  		
	  		reader.close();
	    	} else { 
	    		// Parse HTML files
	    		String doc = parser.getConvertedDocument(file);
	    		processDocument(doc);
	    		
	    		if(_documents.size() % 1000 == 0){
	    			System.out.println("Processed " + _documents.size() + " Documents.");
	    		}
	    		
	    		
	    	}
	    }
	    
	    System.out.println("Processed " + _documents.size() + " Documents.");
	    
	    parser = null;     
	    
	    System.out.println(
	        "Indexed " + Integer.toString(_numDocs) + " docs with " +
	        Long.toString(_totalTermFrequency) + " terms.");
	    
	    storePostings();
	    
	    String indexFile = _options._indexPrefix + "/corpus.idx";
	    System.out.println("Store index to: " + indexFile);
	    ObjectOutputStream writer =
	        new ObjectOutputStream(new FileOutputStream(indexFile));
	    writer.writeObject(this);
	    writer.close();
	  
	}

	public void processDocument(String content) {
	  
		HashMap<Integer, Vector<Integer>> docPostings = new HashMap<Integer, Vector<Integer>>();
		tokenPosition = 0;
	  
		@SuppressWarnings("resource")
		Scanner s = new Scanner(content).useDelimiter("\t");
	  	
		// title
		String title = s.next();
	  
		// Stemmed Title
		String stemmedtitle = s.next();
	    
		readTermVector(stemmedtitle, docPostings);
	    
		// body
		readTermVector(s.next(), docPostings);
	    
		// Number of views
		int numviews = Integer.parseInt(s.next());
	    
		// url
		String url = s.next();
	    
		s.close();
	  	
	  	// create the document
	  	int docId = _documents.size();
	    DocumentIndexed doc = new DocumentIndexed(docId);
	    /*
	    doc.setTitleTokens(null);
	    doc.setBodyTokens(null);
	    */
	    doc.setNumViews(numviews);
	    doc.setUrl(url);
	    doc.setTitle(title);
	    doc.size = tokenPosition;
	    
	    _documents.add(doc);
	    ++_numDocs;
	    
	    for (Integer idx : docPostings.keySet()) {
	    	
	    	//_termDocFrequency.put(idx, _termDocFrequency.get(idx) + 1);
	      
	    	// add this doc to index
	    	Vector<Integer> list = _postings.get(idx);
	    	Vector<Integer> idxList = docPostings.get(idx);
	      
	      
	    	Vector<Integer> skipPointList = _skipPointers.get(idx); 
	    	//skipPointList.add(docId);
	    	skipPointList.add(list.size());
	      
	      
	    	list.add(docId);
	    	list.add(idxList.size());
	    	list.addAll(idxList);
	  	  	_postings.put(idx, list);
	    }	    
	}
	  
	private void readTermVector(String content, HashMap<Integer, Vector<Integer>> docPostings) {
	  
		Scanner s = new Scanner(content);  // Uses white space by default.
	  
		while (s.hasNext()) {
			
			String token = s.next();
			int idx = -1;
		  
			if (_dictionary.containsKey(token)) {
				idx = _dictionary.get(token);
			} else {
	    	  
				idx = _dictionary.size();
				_dictionary.put(token, idx);
				_termCorpusFrequency.put(idx, 0);
				//_termDocFrequency.put(idx, 0);
				_postings.put(idx, new Vector<Integer>());
	        
			}
	      
			if(!docPostings.containsKey(idx)) {
				docPostings.put(idx, new Vector<Integer>());
			}
	      
			if(!_skipPointers.containsKey(idx)) {
				_skipPointers.put(idx, new Vector<Integer>());
			}
			
			
			
			Vector<Integer> positions = docPostings.get(idx);
			positions.add(tokenPosition);
			docPostings.put(idx, positions);
			tokenPosition++;
	      
			_termCorpusFrequency.put(idx, _termCorpusFrequency.get(idx) + 1);
			++_totalTermFrequency;
		  
		}
		s.close();
	}
  
	public void storePostings() throws FileNotFoundException, IOException {
		
		System.out.println("Store postings in " +_options._indexPrefix);
		
		for(int i=0; i<= this._postings.size() / BUF_SIZE; i++) {
			
			TreeMap<Integer, Vector<Integer>> m = new TreeMap<Integer, Vector<Integer>>(this._postings.subMap(i*BUF_SIZE, Math.min((i+1)*BUF_SIZE, this._postings.size())));
			
			String postingFile =  _options._indexPrefix + "/postings" + i + ".idx";
		    ObjectOutputStream writer =
		        new ObjectOutputStream(new FileOutputStream(postingFile));
		    writer.writeObject(m);
		    writer.close();
			
		}
		
		this._postings.clear();
		this._postings = null;
		
	}
	    	
	    
	@Override
	public void loadIndex() throws IOException, ClassNotFoundException {
		
		String indexFile = _options._indexPrefix + "/corpus.idx";
	    System.out.println("Load index from: " + indexFile);
	    
	    ObjectInputStream reader =
	        new ObjectInputStream(new FileInputStream(indexFile));
	    
	    IndexerInvertedOccurrence loaded = (IndexerInvertedOccurrence) reader.readObject();
	    reader.close();
	    reader = null;
	    
	    this._documents = loaded._documents;
	    
	    // Compute numDocs and totalTermFrequency b/c Indexer is not serializable.
	    this._numDocs = _documents.size();
	    
	    for (Integer freq : loaded._termCorpusFrequency.values()) {
	      this._totalTermFrequency += freq;
	    }

	    
	    
	    
	    // TODO : Check if _terms is really needed.
	    //this._terms = loaded._terms;
	    this._termCorpusFrequency = loaded._termCorpusFrequency;
	    this._termDocFrequency = loaded._termDocFrequency;
	    
	    this._skipPointers = loaded._skipPointers;
	   
	    this._dictionary = loaded._dictionary;
	    loaded = null;
	    
	    System.out.println(Integer.toString(_numDocs) + " documents loaded " +
	        "with " + Long.toString(_totalTermFrequency) + " terms!");
	    
	    
	    
	    for(int i=0; i<FIXED_SIZE; i++) {
			
			String postingsFile =  _options._indexPrefix + "/postings" + i + ".idx";
		    //System.out.println("Load postings from: " + postingsFile);
		    
		    reader =
		        new ObjectInputStream(new FileInputStream(postingsFile));
		    
		    @SuppressWarnings("unchecked")
			TreeMap<Integer, Vector<Integer>> loadedPostings = (TreeMap<Integer, Vector<Integer>>) reader.readObject();
		    
		    _FixedPostings.putAll(loadedPostings);
		    
		    reader.close();
			
		}
	    
	}

	@Override
	public Document getDoc(int docid) {
	  
		if(docid >= _documents.size() && docid < 0) {
			return null;
	    } else {
	    	return _documents.get(docid);
	    }
	}

	/**
	 * In HW2, you should be using {@link DocumentIndexed}.
	 */
	@Override
	public Document nextDoc(Query query, int docid) {
    
		
		for(String token: query._tokens){
			if(!_dictionary.containsKey(token)){
				return null;
			}
		}
		
		try {
			loadPostings(query);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		QueryPhrase qp = ((QueryPhrase)query);
		Vector<String> queryTokens = qp._tokens;
		boolean flag = true;
	  
		int i = 0;
		int prevDocId = -1;
		int maxDocId = -1;
	  
	  
		for(String token: queryTokens) {
		  
			int nextDocId = nextDocId(token, docid);
		  
			if(nextDocId == -1) {
				return null;
			}
		  
			if(i == 0) {
				prevDocId = nextDocId;
			} else if (prevDocId != nextDocId) {
				flag = false;
			} 
		  
			maxDocId = Math.max(maxDocId, nextDocId);
			prevDocId = nextDocId;
			i++;
		  
		}	
	  
		if(flag == true) {
		  
			if(qp.phraseQueries != null && !qp.phraseQueries.isEmpty()) {
			  
				if(checkPhrases(qp.phraseQueries,maxDocId)) {
					return _documents.get(maxDocId);
				} else {
					return nextDoc(query, maxDocId);
				}
			  
			} else {
			  
				return _documents.get(maxDocId);
			}
		  
		} 
		  
		return nextDoc(query, maxDocId-1);
	  
	    
		//return null;
	}
  
  
	public Boolean checkPhrases(Vector<Vector<String>> phrases, int docId){
	  
		for(Vector<String> phrase : phrases) {
		  
			if(!checkPhrase(phrase,docId)){
				return false;
			}

		}
	  
		return true;
	}
  
	public Boolean checkPhrase(Vector<String> phrase, int docId) {
	  
		HashMap<Integer, Integer> cache = new HashMap<Integer, Integer>();
	  
		Vector<Integer> checkPositions= new Vector<Integer>();
	  
		boolean flag = true;
	  
		int prevPos = -1;
	  
		checkPositions.add(-1);
	  
		while(true) {
			flag = true;
			int i = 0;
			prevPos = checkPositions.get(0);
			for(String str: phrase){
				
				if(prevPos == -1 || checkPositions.get(i) <= prevPos) {
				
					int newPos = nextPos(str, docId, prevPos, cache);
					if(newPos == -1) {
						return false;
					}
				
					checkPositions.add(i,newPos);
				}
			
				if(i!=0) {
					
					if(checkPositions.get(i-1) == checkPositions.get(i)) {
						checkPositions.add(i,nextPos(str, docId, checkPositions.get(i), cache));
					}
				
					if(checkPositions.get(i-1) + 1!= checkPositions.get(i)) {
						flag = false;
					}
				} else {
					prevPos = checkPositions.get(i);
				}
			
				i++;
			}	
		
			if(flag == true) {
				return true;
			}
		
		}
	  
		//return true;
	}
	
	
	public int nextPos(String term, int docId, int prevPos, HashMap<Integer, Integer> cache) {
		
		if(!_dictionary.containsKey(term)){
			return -1; 
		}
		
		int idx = _dictionary.get(term);
		int docPos = -1;
		
		if(!cache.containsKey(idx)) {
			docPos = exactBinarySearch(idx, docId);
			if(docPos == -1) {
				return -1;
			}
		  
			cache.put(idx, docPos);
			
		} else {
			
			docPos = cache.get(idx);
		}
		
		Vector<Integer> posts = _postings.get(idx);
		int count = posts.get(docPos+1);
		
		int low = docPos + 2;
		int high = docPos + 1 + count;
		
		if(posts.get(low) > prevPos) {
			return posts.get(low);
		}
	  
		if(posts.get(high) <= prevPos) {
			return -1;
		}	
		
		while(high - low > 1) {
			  
			int mid = (low+high) / 2;
		  
			if(posts.get(mid) <= prevPos) {
				low = mid;
			} else {
				high = mid;
			}
		}
	  
		return posts.get(high);
	}

	@Override
	public int corpusDocFrequencyByTerm(String term) {
    
		return 0;
	  
	}

	@Override
	public int corpusTermFrequency(String term) {
    
		if(_dictionary.containsKey(term)){
			return _termCorpusFrequency.get(_dictionary.get(term));
		} else {
			return 0;
		}

	}

	@Override
	public int documentTermFrequency(String term, int docid) {
	
		return 0;
	}
	
	public int nextDocId(String token, int docid) {
		
		if(!_dictionary.containsKey(token)){
			return -1; 
		}
		
		Vector<Integer> skip = this._skipPointers.get(_dictionary.get(token));
		Vector<Integer> posts = this._postings.get(_dictionary.get(token));
		
		if(posts.get(skip.get(0)) > docid) {
			return posts.get(skip.get(0));
		}
		
		if(posts.get(skip.get(skip.size()-1)) <= docid) {
			return -1;
		}
			
		int low = 0;
		int high = skip.size()-1;

		while( high - low > 1)
	    {

	        int mid = (high + low) / 2;

	        if (posts.get(skip.get(mid)) <= docid) {
	        	low  = mid;
	        } else {
	        	high = mid;
	        }
	    }
		
		return posts.get(skip.get(high));
		
	}
  
  
	private int exactBinarySearch(int idx, int docid) {
	  
		Vector<Integer> posts = _postings.get(idx);
		Vector<Integer> skip = _skipPointers.get(idx);
		
		if(docid < 0 || docid > posts.get(skip.get(skip.size()-1))) {
			return -1;
		}
	  
		int low = 0;
		int high = skip.size()-1;
	  
		while(low<=high) {
		  
			int mid = (low+high) / 2;
		  
			if(posts.get(skip.get(mid)) == docid) {
				return skip.get(mid);
			} else if(posts.get(skip.get(mid)) < docid) {
				low = mid+1;
			} else {
				high = mid - 1;
			}
		  
		}
	  
		return -1;
	}
  
	public void loadPostings(Query query) throws FileNotFoundException, ClassNotFoundException, IOException {
		
		boolean flag = false;
		for(String token: query._tokens) {
			int idx = _dictionary.get(token);
			
			if(!_postings.containsKey(idx)) {
				
				flag = true;
				break;
			}	
			
		}
		
		if(flag == false) {
			return;
		}
		
		
		_postings = null;
		_postings = new TreeMap<Integer,Vector<Integer>>(_FixedPostings);
		
		for(String token: query._tokens) {
			int idx = _dictionary.get(token);
			
			if(!_postings.containsKey(idx)) {
				
				addPosting(idx);
			}	
			
		}
	}
	
	public void addPosting(int idx) throws FileNotFoundException, IOException, ClassNotFoundException{
		int i = idx / BUF_SIZE;
		
		String postingsFile =  _options._indexPrefix + "/postings" + i + ".idx";
	    
		ObjectInputStream reader =
	        new ObjectInputStream(new FileInputStream(postingsFile));
	    
	    @SuppressWarnings("unchecked")
		TreeMap<Integer, Vector<Integer>> loadedPostings = (TreeMap<Integer, Vector<Integer>>) reader.readObject();
	    
	    _postings.putAll(loadedPostings);
	    
	    reader.close();
	}
}
