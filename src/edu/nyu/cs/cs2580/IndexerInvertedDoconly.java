package edu.nyu.cs.cs2580;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.Vector;

import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Implement this class for HW2.
 */
public class IndexerInvertedDoconly extends Indexer implements Serializable {

	private static final long serialVersionUID = -5701664574809285090L;
	
	// Maps each term to their integer representation
	private Map<String, Integer> _dictionary = 
			new HashMap<String, Integer>();
	
	// Inverted Index
	private Map<Integer, Vector<Integer>> _postings = 
			new HashMap<Integer, Vector<Integer>>();
  
	// Term document frequency, key is the integer representation of the term and
	// value is the number of documents the term appears in.
	// NOT USED
	private Map<Integer, Integer> _termDocFrequency =
     new HashMap<Integer, Integer>();
	
	// Term frequency, key is the integer representation of the term and value is
	// the number of times the term appears in the corpus.
	private Map<Integer, Integer> _termCorpusFrequency =
     new HashMap<Integer, Integer>();
	
	// Stores all Document in memory.
	private Vector<DocumentIndexed> _documents = new Vector<DocumentIndexed>();
	
	// cached index offset
	private int ct = -1;
	
	// used to store last index offset for each query token
	private Map<String, Integer> cache = new HashMap<String, Integer>();
	
	/* TODO : NOT NEEDED
	// maps document to all the terms in that document and their respective
	// frequencies in the document
	private Map<Integer, HashMap<Integer, Integer>> _documentTermFrequency =
			new HashMap<Integer, HashMap<Integer, Integer>>();
			*/
	
	// stores term and its frequency in single document
	private Map<Integer, Integer> _termFrequency = 
			new HashMap<Integer, Integer>();
	
	//All unique terms appeared in corpus. Offsets are integer representations.
	// TODO : Commented as not needed
	//private Vector<String> _terms = new Vector<String>();
	
	// Provided for serialization
	public IndexerInvertedDoconly() { }

	public IndexerInvertedDoconly(Options options) {
    super(options);
    System.out.println("Using Indexer: " + this.getClass().getSimpleName());
  }

	/**
	 * Constructs the index from Corpus File
	 */
  @Override
  public void constructIndex() throws IOException {
  	RawDocumentParser parser = new RawDocumentParser();
  	String corpusFile = _options._corpusPrefix;
    System.out.println("Constructing index documents in: " + corpusFile);
    
    File folder = new File(corpusFile);
    int doc_count = 0;
    for (File file : folder.listFiles()) {
      if (file.isHidden() || file.isDirectory()) {
      	continue;	
      }
      if (!file.isFile()) {
      	continue;
      }
   
      // Parse Sample file
      if (file.getName().endsWith("corpus.tsv") ) {
  		  System.out.println(file.getName());
  		  BufferedReader reader = new BufferedReader(new FileReader(file));
  		  String line = null;
  		  while ((line = reader.readLine()) != null) {
  		  	System.out.println("Document" + doc_count);
  		    line = parser.getConvertedDocument(line);
  		    processDocument(line); 			
  		    doc_count++;
  		  }
  		  reader.close();
  	  } else { // Parse HTML files
  	  	if (doc_count % 1000 == 0) {
  	  		System.out.println("Processed " + doc_count + " documents");
  	  	}
  	  	doc_count++;
  	  	String doc = parser.getConvertedDocument(file);
  	  	processDocument(doc);
  	  }
    }
    
    folder = null;
    parser = null;
    
    System.out.println("Processed " + _numDocs + " documents");
    
    System.out.println(
        "Indexed " + Integer.toString(_numDocs) + " docs with " +
        Long.toString(_totalTermFrequency) + " terms.");
    
    
    String indexFile = _options._indexPrefix + "/corpus.idx";
    System.out.println("Store index to: " + indexFile);
    ObjectOutputStream writer =
        new ObjectOutputStream(new FileOutputStream(indexFile));
    writer.writeObject(this);
    _postings = null;
    
    /*
    // write postings
    String indexFile = _options._indexPrefix + "/postings.idx";
    System.out.println("Store index to: " + indexFile);
    ObjectOutputStream writer =
        new ObjectOutputStream(new FileOutputStream(indexFile));
    writer.writeObject(this._postings);
    _postings = null;
    
    
    // write remaining data structures
    indexFile = _options._indexPrefix + "/corpus.idx";
    System.out.println("Store index to: " + indexFile);
    writer =
        new ObjectOutputStream(new FileOutputStream(indexFile));
    writer.writeObject(this);
    */
    writer.close();
    writer = null;    
  }
  
  /**
   * Process the raw content (i.e., one line in corpus.tsv) corresponding to a
   * document, and constructs the token vectors for both title and body.
   * @param content : String containing title, stemmed title, stemmed body, numviews
   * and URL
   */
  private void processDocument(String content) {
  	Scanner s = new Scanner(content);
  	s.useDelimiter("\t");
  	
  	int size = 0;
  	
  	// title (Non-stemmed)
  	String title = s.next();
  	
  	// title (Stemmed)
  	Vector<Integer> titleTokens = new Vector<Integer>();
    size = size + readTermVector(s.next(), titleTokens);
    
    // body (Stemmed)
    Vector<Integer> bodyTokens = new Vector<Integer>();
    size = size + readTermVector(s.next(), bodyTokens);
    
    // Number of views
    int numviews = Integer.parseInt(s.next());
    
    // url
    String url = s.next();
    
  	s.close();
  	
  	s = null;
  	
  	// create the document   
    DocumentIndexed doc = new DocumentIndexed(_documents.size());
    
    doc.setNumViews(numviews);
    doc.setUrl(url);
    doc.setTitle(title);
    doc.size = size;
    
    _documents.add(doc);
    ++_numDocs;
    
    Set<Integer> uniqueTerms = new HashSet<Integer>();
    
    updateStatistics(titleTokens, uniqueTerms);
    updateStatistics(bodyTokens, uniqueTerms);
        
    for (Integer idx : uniqueTerms) {
      //_termDocFrequency.put(idx, _termDocFrequency.get(idx) + 1);
      // add this doc to index
      Vector<Integer> list = _postings.get(idx);
      list.add(_documents.size() - 1);
  	  _postings.put(idx, list);
    }

    uniqueTerms = null;
    bodyTokens = null;
    titleTokens = null;
    _termFrequency.clear();    
  }
  
  /**
   * Tokenize {@code content} into terms, translate terms into their integer
   * representation, store the integers in {@code tokens}.
   * @param content
   * @param tokens
   */
  private int readTermVector(String content, Vector<Integer> tokens) {
  	Scanner s = new Scanner(content);  // Uses white space by default.
    int size = 0;
  	while (s.hasNext()) {
      String token = s.next();
      size++;
      int idx = -1;
      if (_dictionary.containsKey(token)) {
        idx = _dictionary.get(token);
      } else {
        idx = _dictionary.size();
        _dictionary.put(token, idx);
        _termCorpusFrequency.put(idx, 0);
        //_termDocFrequency.put(idx, 0);
        _postings.put(idx,new Vector<Integer>());
      }
      tokens.add(idx);
    }
    s.close();
    s = null;
    return size;
  }
  
  /**
   * Update the corpus statistics with {@code tokens}. Using {@code uniques} to
   * bridge between different token vectors.
   * @param tokens
   * @param uniques
   */
  private void updateStatistics(Vector<Integer> tokens, Set<Integer> uniques) {
    for (int idx : tokens) {
    	if (_termFrequency.containsKey(idx)) {
    		_termFrequency.put(idx, _termFrequency.get(idx) + 1);
    	} else {
    		_termFrequency.put(idx, 1);
    	}
      uniques.add(idx);
      _termCorpusFrequency.put(idx, _termCorpusFrequency.get(idx) + 1);
      ++_totalTermFrequency;
    }
  }

  @Override
  public void loadIndex() throws IOException, ClassNotFoundException {
  	// Read Postings
  	String indexFile = _options._indexPrefix + "/corpus.idx";
    System.out.println("Load index from: " + indexFile);
    ObjectInputStream reader =
        new ObjectInputStream(new FileInputStream(indexFile));
    IndexerInvertedDoconly loaded = 
    		(IndexerInvertedDoconly) reader.readObject();
    reader.close();
    reader = null;
    this._postings = loaded._postings;
    System.out.println("Loaded _postings");
    this._documents = loaded._documents;
    System.out.println("Loaded _documents");
    // Compute numDocs and totalTermFrequency b/c Indexer is not serializable.
    this._numDocs = _documents.size();
    System.out.println("Loaded _numDocs");
    for (Integer freq : loaded._termCorpusFrequency.values()) {
      this._totalTermFrequency += freq;
    }
    System.out.println("Loaded _termCorpusFrequency");
    this._dictionary = loaded._dictionary;
    System.out.println("Loaded _dictionary");
    this._termCorpusFrequency = loaded._termCorpusFrequency;
    System.out.println("Loaded _termCorpusFrequcny");
    this._termDocFrequency = loaded._termDocFrequency;
    System.out.println("Loaded _termDocFrequency");
    loaded = null;
    
    System.out.println(Integer.toString(_numDocs) + " documents loaded " +
        "with " + Long.toString(_totalTermFrequency) + " terms!");
  }

  @Override
  public Document getDoc(int docid) {
  	return (docid >= _documents.size() || docid < 0) ? null : _documents.get(docid);
  }

  /**
   * In HW2, you should be using {@link DocumentIndexed}
   */
  @Override
  public Document nextDoc(Query query, int docid) {
  	Vector<Double> document = new Vector<Double>();

  	int size = query._tokens.size();
  	if (size == 0) {
  		return null;
  	}
  	for (int i = 0; i < size; i++) {
  		String token = query._tokens.get(i);
  		if (cache.containsKey(token)) {
  			ct = cache.get(token);
  		} else {
  			ct = -1;
  		}
  		document.add(i, next(token, docid));
  		cache.put(token, ct);
  		
  		if (document.get(i).equals(Double.POSITIVE_INFINITY)) {
  			return null;
  		}
  	}
  	
  	if (Collections.max(document) == Collections.min(document)) {
  		return _documents.get(document.get(0).intValue());
  	}
  	
  	return nextDoc(query, Collections.max(document).intValue() - 1);
  }

  private Double next(String t, int current) {
  	if (!_dictionary.containsKey(t)) {
  		return Double.POSITIVE_INFINITY;
  	}
  	
  	Vector<Integer> pt = new Vector<Integer>();
  	pt = _postings.get(_dictionary.get(t));
  	
  	int lt = pt.size()-1;
  	
  	if (lt == -1 || pt.get(lt) <= current)
  		return Double.POSITIVE_INFINITY;
  	
  	if (pt.get(0) > current) {
  		ct = 0;
  		return (1.0 * pt.get(0));
  	}
  	
  	int low = 0;
  	
  	if (ct > 0 && pt.get(ct - 1) <= current) {
  		low = ct - 1;
  	} else {
  		low = 0;
  	}
  	
  	int jump = 1;
  	int high = low + jump;
  	
  	while (high < lt && pt.get(high) <= current) {
  		low = high;
  		jump = 2 * jump;
  		high = low + jump;
  	}
  	
  	if (high > lt) {
  		high = lt;
  	}
  	
  	ct = binarySearch(t, low, high, current, pt);
  	
  	return (1.0 * pt.get(ct));
	}

	private int binarySearch(String t, int low, int high, int current, Vector<Integer> pt) {
		while (high - low > 1) {
			int mid = low + (int) Math.floor(((high - low) / 2));
			if (pt.get(mid) <= current) {
				low = mid;
			} else {
				high = mid;
			}
		}
		return high;
	}

	@Override
  public int corpusDocFrequencyByTerm(String term) {
  	return _dictionary.containsKey(term) ?
        _termDocFrequency.get(_dictionary.get(term)) : 0;
  }

  @Override
  public int corpusTermFrequency(String term) {
  	return _dictionary.containsKey(term) ?
        _termCorpusFrequency.get(_dictionary.get(term)) : 0;
  }

  @Override
  public int documentTermFrequency(String term, int docid) {
  	SearchEngine.Check(false, "Not implemented!");
    return 0;
  }
  
  /**
   * Protected Access! Used for testing.
   */
  void dumpInvertedIndex() {
  	System.out.println(_postings);
  }
  
  void dumpDictionary() {
  	System.out.println(_dictionary);
  }
  
  void dumpTermDocFrequency() {
  	System.out.println(_termDocFrequency);
  }
  
  void dumpTermCorpusFrequency() {
  	System.out.println(_termCorpusFrequency);
  }
}
