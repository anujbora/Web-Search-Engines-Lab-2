package edu.nyu.cs.cs2580;

import java.util.Vector;

import edu.nyu.cs.cs2580.QueryHandler.CgiArguments;
import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * Instructors' code for illustration purpose. Non-tested code.
 * 
 * @author congyu
 */
public class RankerConjunctive extends Ranker {

  public RankerConjunctive(Options options,
      CgiArguments arguments, Indexer indexer) {
    super(options, arguments, indexer);
    System.out.println("Using Ranker: " + this.getClass().getSimpleName());
  }

  @Override
  public Vector<ScoredDocument> runQuery(Query query, int numResults) {
    //Queue<ScoredDocument> rankQueue = new PriorityQueue<ScoredDocument>();
    Vector<ScoredDocument> rankList = new Vector<ScoredDocument>();
    Document doc = null;
    int docid = -1;
    while ((doc = _indexer.nextDoc(query, docid)) != null) {
      /*
    	rankQueue.add(new ScoredDocument(doc, 1.0));
      if (rankQueue.size() > numResults) {
        rankQueue.poll();
      }
      */
    	/*rankList.add(new ScoredDocument(doc, 1.0));
    	if (rankList.size() > numResults) {
    		rankList.remove(0);
    	} */
    	rankList.addElement(new ScoredDocument(doc, 1.0));
    	if (rankList.size() == numResults) {
    		break;
    	}
    	docid = doc._docid;
    }

    //Vector<ScoredDocument> results = new Vector<ScoredDocument>();
    //ScoredDocument scoredDoc = null;
    /*
    while ((scoredDoc = rankQueue.poll()) != null) {
      results.add(scoredDoc);
    }
    */
    
    /*
    while (!rankList.isEmpty()) {
    	results.add(rankList.get(0));
    	rankList.remove(0);
    } */

    //Collections.sort(results, Collections.reverseOrder());
    return rankList;
  }
}