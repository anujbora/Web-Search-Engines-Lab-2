package edu.nyu.cs.cs2580;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * 
 *
 */
public class RawDocumentParser {
	
	public RawDocumentParser() { }
	
	/**
	 * Converts the given string into string representation as needed by the
	 * indexer by removing formatting the contents as :
	 * title \t stemmed title \t stemmed body \t numviews \t url
	 * @param string
	 * @return formatted string
	 * @throws IOException 
	 */
	public String getConvertedDocument(String file) throws IOException {
		StringBuilder result = new StringBuilder();
		Scanner s = new Scanner(file);
		s.useDelimiter("\t");
		String title = s.next();
		String stemmedTitle = cleanAndStem(title);
		String stemmedBody = cleanAndStem(s.next());
		int numViews = Integer.parseInt(s.next());

		result.append(title);
		result.append('\t');
		result.append(stemmedTitle);
		result.append('\t');
		result.append(stemmedBody);
		result.append('\t');
		result.append(numViews);
		result.append('\t');
		result.append("https://en.wikipedia.org/wiki/" + title.replace(" ", "_"));	//URL
		
		s.close();
		return result.toString();
	}
	
	/**
	 * Converts the HTML file into string representation as needed by the
	 * indexer by removing HTML and unnecessary content from the file.
	 * The output contents are formatted as :
	 * title \t stemmed title \t stemmed body \t numviews \t url
	 * @param file
	 * @return string representation of the file
	 * @throws IOException 
	 */
	public String getConvertedDocument(File file) throws IOException {
		StringBuilder result = new StringBuilder();
		String HTMLString = readFile(file);
		
		Document doc = Jsoup.parse(HTMLString);

		/*
		// Append title of the file
		result.append(file.getName().replace("_", " "));
		
		// Append stemmed title of the file
		result.append("\t");
		result.append(cleanAndStem(file.getName().replace("_", " ")));
		 */
		
		String title = getTagText(doc, "title");
		//title = title.replace("- Wikipedia, the free encyclopedia", " ");
		if (title.equals("ERROR:NoTitleSpecified")) {
			title = file.getName().replace("_", " ");
		}
		//System.out.println(title);
		// Append title of the file
		result.append(title);
			
		// Append stemmed title of the file
		result.append("\t");
		result.append(cleanAndStem(title));
		
		// Append stemmed content of the file
		result.append("\t");
		result.append(getTagText(doc, "h1"));
		result.append(" ");
		result.append(getTagText(doc, "p"));
		result.append(" ");
		
		// Append Number of views (zero)
		result.append("\t");
		result.append("0");
		
		// Append URL of the file
		result.append("\t");
		result.append("https://en.wikipedia.org/wiki/" + file.getName());
		
		return result.toString();
	}
	
	private String readFile(File filename) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		StringBuilder contents = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
		    contents.append(line);
		}
		reader.close();
		return contents.toString();
	}
	
	private String getTagText(Document html_doc, String tag) throws IOException {
		StringBuilder tagText = new StringBuilder();
		
		Elements tags = html_doc.getElementsByTag(tag);
		
		if (tags.size() == 0) {
			return " ";
		}
		
		if (tag.equals("title")) {
			String title = tags.first().text();
			if (title.isEmpty()) {
				return "ERROR:NoTitleSpecified";
			}
			return title;
		}
		
		
		for (Element e : tags) {
			//String cleanedSectionText;
			String cleanedSectiontext = cleanAndStem(e.text());
		  tagText.append(cleanedSectiontext);
		  tagText.append(" ");
		} 
		
		return tagText.toString();
	}
	
	/**
	 * @see {@link Stemmer}
	 * @param string to be stemmed
	 * @return stemmed string
	 * @throws IOException
	 */
	public String cleanAndStem(String body) throws IOException {		

		String specialChars = "!\"#$%&\'()*+,-./:;<=>?@[]^\\_`{|}~^";
		body = body.replaceAll("[" + Pattern.quote(specialChars)  + "]", " ");

		StringBuilder out = new StringBuilder();
		char[] w = new char[501];
		Stemmer porter = new Stemmer();
		InputStream in = new ByteArrayInputStream(body.getBytes());
		
		while(true) {
			int ch = in.read();
		  if (Character.isLetter((char) ch)) {
		  	int j = 0;
		    while(true) {
		    	ch = Character.toLowerCase((char) ch);
		    	w[j] = (char) ch;
		    	if (j < 500) {
		    		j++;
		    	}
		    	ch = in.read();
		    	if (!Character.isLetter((char) ch)) {
		    		for (int c = 0; c < j; c++) {
		    			porter.add(w[c]);
		    		}
		    		porter.stem();
		    		out.append(porter.toString());
		    		break;
		    	}
		    }
		  }
		  if (ch < 0) {
		  	break;
		  }
		  out.append((char)ch);
		}
		
		return out.toString();
	}
}