package edu.nyu.cs.cs2580;

/**
 * Document with score.
 * 
 * @author fdiaz
 * @author congyu
 */
class ScoredDocument implements Comparable<ScoredDocument> {
  private Document _doc;
  private double _score;

  public ScoredDocument(Document doc, double score) {
    _doc = doc;
    _score = score;
  }
  
  public double get_score() {
		return _score;
	  }

	  public void set_score(double _score) {
		this._score = _score;
	  }

  public String asTextResult() {
    StringBuffer buf = new StringBuffer();
    buf.append(_doc._docid).append("\t");
    buf.append(_doc.getTitle()).append("\t");
    buf.append(_score).append("\t");
    buf.append(_doc.getUrl());
    return buf.toString();
  }

  /**
   * @CS2580: Student should implement {@code asHtmlResult} for final project.
   */
  public String asHtmlResult() {
  	return "<h2>" + _doc.getTitle() + "</h2>"
    		+ "<h6> Document ID = " + _doc._docid + "&nbsp;&nbsp;" + 
    		"<a href='" + _doc.getUrl() + "'>" + _doc.getUrl() +
    		"</a>";
  }

  @Override
  public int compareTo(ScoredDocument o) {
    if (this._score == o._score) {
      return 0;
    }
    return (this._score > o._score) ? 1 : -1;
  }
}
